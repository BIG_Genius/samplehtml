package com.sample.html;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Browser;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.net.URLEncoder;
import java.util.Locale;

public class ActivityHTML extends AppCompatActivity  implements VerifyPincode {

    private WebView webview;
    public static final String TAG = "ActivityHTML";
    public static ActivityHTML activityHTML;
    private SmsRetrieverClient smsRetrieverClient;
    private MessageReceiver messageReceiver;
    private String url = "";
    private InputMethodManager inputMethodManager ;
    private Context mContext;
    private boolean checkNative = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);
        activityHTML = this;
        mContext = this;
        webview =(WebView) findViewById(R.id.webview);
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.setOverScrollMode(WebView.OVER_SCROLL_NEVER);

        try {
            inputMethodManager =
                    (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (!Utility.isPremiumUser(ActivityHTML.this)) {
                messageReceiver = new MessageReceiver();
                smsretrive();
                IntentFilter filter = new IntentFilter();
                filter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
                registerReceiver(messageReceiver, filter);
            }
        }catch (Exception e){

        }

        url = Utility.getLoadURL(ActivityHTML.this) +"?";
        url = url+ "packageName="+Utility.getPackageName(ActivityHTML.this)
                + "&osVersion="+Utility.getOsVersion()
                + "&appVersionCode="+Utility.getAppVersionCode()
                + "&typeOfBuild="+Utility.getTypeOfBuild()
                + "&model="+Utility.getModel()
                + "&lang="+Locale.getDefault().getLanguage()
                + "&deviceId="+Utility.getDeviceIdFromDevice(ActivityHTML.this)
                + "&isPremium="+Utility.isPremiumUser(ActivityHTML.this)
                + "&gpsAdid="+Utility.getAdvertisingID(ActivityHTML.this)
                + "&referringLink=" + Utility.getReferringLink(ActivityHTML.this)
                + "&platformId=1" ;

        if (Utility.isPremiumUser(ActivityHTML.this)){
            try {
                url = url +"&thankYouUrl=" +  URLEncoder.encode(Utility.getThankYouURL(ActivityHTML.this),"UTF-8");
            }catch (Exception e){

            }
        }
        webview.requestFocus(View.FOCUS_DOWN);
        webview.loadUrl(url);

        Logcat.e(TAG,"url: "+url);

        webview.setWebChromeClient(new WebChromeClient());

        webview.setWebViewClient(new WebViewClient() {
            String result = "";

            @Override
            public void onPageFinished(WebView view, String url) {
                Logcat.e(TAG, "onPageFinished url: " + url);
                if (!Utility.isPremiumUser(mContext)){
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            if(!checkNative) {
                               final String phoneFocus = "javascript:document.getElementById('phone');";
                                 if (Build.VERSION.SDK_INT >= 19) {
                                    webview.evaluateJavascript(phoneFocus, new ValueCallback<String>() {
                                        @Override
                                        public void onReceiveValue(String s) {
                                            Logcat.e(TAG, "phoneFocus s: " + s);
                                            try {
                                                if (s !=null && !s.equals("null")){
                                                    inputMethodManager.showSoftInput(webview, InputMethodManager.SHOW_IMPLICIT);
                                                }
                                            }catch (Exception e){

                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }, 5000);

                }

                super.onPageFinished(view, url);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Logcat.e(TAG, "shouldOverrideUrlLoading url: " + url);

                Uri uri = Uri.parse(url);
                if (url != null && url.startsWith("sms:")) {
                    Logcat.e(TAG, "result sms" );
                    webview.getContext().startActivity(new Intent(Intent.ACTION_SENDTO, uri));
                    return true;
                }

                try {
                    //save openExt
                    String openExt = uri.getQueryParameter("openExt");
                    if (openExt != null && openExt.length() > 0) {
                        Logcat.e(TAG, "result openExt->" + openExt);
                        if (openExt.toLowerCase().contains("true")){
                            Utility.setOpenExt(ActivityHTML.this, true);
                        }
                    }
                    result = uri.getQueryParameter("result");
                    Logcat.e(TAG, "result res->" + result);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }

                if (result != null && result.equalsIgnoreCase("SUCCESS") && url.contains("Thankyou.html")) {
                    try {
                        String reportToFb = uri.getQueryParameter("reportToFb");
                        if (reportToFb != null && reportToFb.length() > 0 && reportToFb.toLowerCase().contains("true") && !Utility.isPremiumUser(ActivityHTML.this))
                            sendFBEvent();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                    Utility.setPremiumUser(ActivityHTML.this,true);
                    Utility.setThankYouURL(ActivityHTML.this, url);
                    if (Utility.isOpenExt(ActivityHTML.this)){
                        OpenPortalInExternalBrowser(url + "&isPremium=true");
                    }else{
                        openPortalInAppBrowser(url + "&isPremium=true");
                    }
                } else if (result != null && result.equalsIgnoreCase("Failed")  && url.contains("Thankyou.html")) {
                    if (Utility.isOpenExt(ActivityHTML.this)){
                        OpenPortalInExternalBrowser(url + "&isPremium=false");
                    }else{
                        openPortalInAppBrowser(url + "&isPremium=false");
                    }
                }else if (url.contains("checknative.html")) {
                    checkNative = true;
                    Intent intent = new Intent(ActivityHTML.this, MainActivity.class);
                    intent.putExtra("checkNative","Yes");
                    startActivity(intent);
                    finish();
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webview.canGoBack()) {
                        webview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public static ActivityHTML getInstance(){
        return activityHTML;
    }

    private void openPortalInAppBrowser(final String url){
        Logcat.e(TAG, "openPortalInAppBrowser: "+url);
        Intent intent=new Intent(ActivityHTML.this,WebviewActivity.class);
        intent.putExtra("link", url);
        startActivity(intent);
        finish();
    }
    private void OpenPortalInExternalBrowser(String url){
        Logcat.e(TAG, "OpenPortalInExternalBrowser: "+url);
        final Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.putExtra(Browser.EXTRA_APPLICATION_ID, mContext.getPackageName());
        try {
            intent.setPackage("com.android.chrome");
            startActivity(intent);
        }catch (Exception e){
            intent.setPackage(null);
            startActivity(intent);
        }
        finish();
    }
    @Override
    protected void onPause() {
        try {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
            }
        }catch (Exception e){

        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        try {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
            }
        }catch (Exception e){

        }
        try {
            unregisterReceiver(messageReceiver);
        }catch (Exception e){

        }

        super.onDestroy();
    }
    public void smsretrive(){
        smsRetrieverClient = SmsRetriever.getClient(ActivityHTML.this);

        Task<Void> task = smsRetrieverClient.startSmsRetriever();

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e(TAG, "SmsRetrievalResult status: Success");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "SmsRetrievalResult start failed.", e);
            }
        });
    }

    @Override
    public void fillSMS(final String sms) {
        Log.e(TAG, "fillSMS sms"+ sms);
        final String js = "javascript:document.getElementById('smstext').value = '" + sms + "'" + ";";
        if (Build.VERSION.SDK_INT >= 19) {
            webview.evaluateJavascript(js, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                    Log.e(TAG, "fillSMS onReceiveValue: " + s);

                    final String click = "javascript:document.getElementById('parsepin').click();";
                    if (Build.VERSION.SDK_INT >= 19) {
                        webview.evaluateJavascript(click, new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String s) {
                                Log.e(TAG, "fillSMS click onReceiveValue: " + s);

                            }
                        });
                    }

                }
            });
        }
    }
    public void sendFBEvent(){
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent(AppEventsConstants.EVENT_NAME_SUBSCRIBE);
        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION);
    }
}
