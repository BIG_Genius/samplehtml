package com.sample.html;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class HttpPostClient {
	private static final String TAG = "HttpPostClient";
	public static JSONObject sendHttpPost(String URLs, JSONObject jsonObjSend) {


		JSONObject jsonObjectResp = null;

		try {
			MediaType JSON = MediaType.parse("application/json; charset=utf-8");
			OkHttpClient client = new OkHttpClient();

			RequestBody body = RequestBody.create(JSON, jsonObjSend.toString());

			okhttp3.Request request = new okhttp3.Request.Builder()
					.url(URLs)
					.post(body)
					.build();
			okhttp3.Response response = client.newCall(request).execute();
			String networkResp = response.body().string();

			if (!networkResp.isEmpty()) {
				jsonObjectResp = parseJSONStringToJSONObject(networkResp);

			}
		} catch (Exception ex) {
			String err = String.format("{\"result\":\"false\",\"error\":\"%s\"}", ex.getMessage());
			Logcat.e("httppost","httppost ex :"+ex.toString());
			jsonObjectResp = parseJSONStringToJSONObject(err);
		}
		return jsonObjectResp;
	}
	private static JSONObject parseJSONStringToJSONObject(final String strr) {
		Logcat.e("result ","parseJSONStringToJSONObject :"+strr);
		JSONObject response = null;
		try {
			response = new JSONObject(strr);
		} catch (Exception ex) {
			try {
				response = new JSONObject();
				response.put("result", "failed");
				response.put("data", strr);
				response.put("error", ex.getMessage());
			} catch (Exception exx) {
			}
		}
		return response;
	}
	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}