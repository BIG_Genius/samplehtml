package com.sample.html;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceData {

	public static final String PREFS_SETTINGS = "MyAppPreff";
	public static final String KEY_REFERRING_LINK = "referringLink";
	public static final String KEY_THANK_YOU_URL = "thankYouUrl";
	public static final String KEY_PACKAGE_ID = "packageId";
	public static final String KEY_DEVICE_ID = "deviceId";
	public static final String KEY_IS_PREMIUM = "isPremium";
	public static final String KEY_IS_DEEPLINK_USER = "isDeeplinkUser";
	public static final String KEY_OPENEXT = "openExt";
	public static final String KEY_ADVERTISING_ID = "AdvertisingId";
	public static final String KEY_DEEPLINK_OBJECT = "deeplinkobj";
	public static final String KEY_ADD_TO_INSTALLS = "addtoinstall";
	public static final String KEY_ID_INSTALL = "idinstall";
	public static final String KEY_ENDP = "endp";
	public static final String KEY_LOAD_URL = "loadUrl";

	public static void setStringPrefs(String prefKey, Context context,
									  String Value) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFS_SETTINGS, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(prefKey, Value);
		editor.commit();
	}

	public static String getStringPrefs(String prefKey, Context context) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFS_SETTINGS, 0);
		return settings.getString(prefKey, "");
	}

	public static void setBooleanPrefs(String prefKey, Context context,
									   Boolean value) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFS_SETTINGS, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(prefKey, value);
		editor.commit();
	}

	public static boolean getBooleanPrefs(String prefKey, Context context) {
		SharedPreferences settings = context.getSharedPreferences(
				PREFS_SETTINGS, 0);
		return settings.getBoolean(prefKey, false);
	}

}
