package com.sample.html;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import io.branch.referral.Branch;

public class GlobleApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();


        Branch.enableLogging();
        Branch.getAutoInstance(this).enableFacebookAppLinkCheck();



    }

}