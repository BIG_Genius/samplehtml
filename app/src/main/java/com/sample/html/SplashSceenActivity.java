package com.sample.html;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.Locale;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;


public class SplashSceenActivity extends AppCompatActivity {
    public static final String tag = "SplashScreenActivity";
    private TextView mTextViewVersion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_splashscreen);
        } catch (Exception e) {
            Logcat.e(tag, "home error " + e.toString());
        }

        try {
            getSupportActionBar().hide();
        }catch (Exception e){

        }
        try {
            new AppSignatureHelper(SplashSceenActivity.this).getAppSignatures(SplashSceenActivity.this).get(0);
        } catch (Exception e) {
            Log.e(tag, "Exception: " + e.toString());
        }
    }

    private void navigateToApp() {

        if (Utility.getAdvertisingID(SplashSceenActivity.this) != null && !Utility.getAdvertisingID(SplashSceenActivity.this).equals("")){
            sendToTheHome();
        }else{
            new AsyncTask<Context, Void, String>() {
                @Override
                protected String doInBackground(Context... params) {

                    Context innerContext = params[0];
                    String innerResult = Utility.generateAdvertisingId(innerContext);
                    Logcat.e("info", "GoogleAdId read " + innerResult);
                    Utility.setAdvertisingID(innerContext,innerResult);
                    return innerResult;
                }

                @Override
                protected void onPostExecute(String playAdiId) {
                    Logcat.e("gpsadid", "id->" + playAdiId);
                    Utility.setAdvertisingID(SplashSceenActivity.this,playAdiId);
                    sendToTheHome();
                }
            }.execute(SplashSceenActivity.this);
        }
    }
    
    private void sendToTheHome() {
        if (Utility.isDeeplinkUser(SplashSceenActivity.this)){
           Intent intent = new Intent(SplashSceenActivity.this, ActivityHTML.class);
           startActivity(intent);
       } else {
           Intent intent = new Intent(SplashSceenActivity.this, MainActivity.class);
           startActivity(intent);
       }
       finish();
    }
    public void setupVersionDetails(){
        try {
            mTextViewVersion = findViewById(R.id.textview_version);
            mTextViewVersion.setText("Version: "+BuildConfig.VERSION_NAME);
        }catch (Exception e){
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        setupVersionDetails();
        Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {

                if (error == null) {
                    Logcat.e("BRANCH SDK", referringParams.toString());
                    JSONObject sessionParams = Branch.getInstance().getLatestReferringParams();
                    Logcat.e("sessions params :",sessionParams.toString());
                    processDeepLinkParams(sessionParams);
                } else {

                    Logcat.e("BRANCH SDK", error.getMessage());
                }
                navigateToApp();
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    public void processDeepLinkParams(JSONObject deeplinkObj){
        try {
            if(!Utility.isPremiumUser(SplashSceenActivity.this)){

                if((deeplinkObj.has("source") && deeplinkObj.get("source").toString().equals("ma")) ||
                        (deeplinkObj.has("OpenPage") && deeplinkObj.get("OpenPage").toString().equals("BScreenActivity"))){

                    try {
                        Utility.getReferringLinkFromDeeplink(deeplinkObj,SplashSceenActivity.this);
                    }catch (Exception e){

                    }
                    try {
                        Utility.setDeeplinkObject(SplashSceenActivity.this, deeplinkObj.toString().replace("\\", ""));
                    }catch (Exception e){

                    }
                    Utility.setLogs(deeplinkObj);
                    try {
                        if (deeplinkObj != null && deeplinkObj.has("endp") && !deeplinkObj.isNull("endp")) {
                            Utility.saveEndp(SplashSceenActivity.this, deeplinkObj.get("endp").toString());

                            if (Utility.getAdvertisingID(SplashSceenActivity.this) != null && Utility.getAdvertisingID(SplashSceenActivity.this) != "") {
                                new  CallInstallApi(SplashSceenActivity.this).execute();
                            } else {
                                new AsyncTask<Context, Void, String>() {
                                    @Override
                                    protected String doInBackground(Context... params) {

                                        Context innerContext = params[0];
                                        String innerResult = Utility.generateAdvertisingId(innerContext);
                                        Logcat.e("info", "GoogleAdId read " + innerResult);
                                        Utility.setAdvertisingID(innerContext,innerResult);
                                        return innerResult;
                                    }

                                    @Override
                                    protected void onPostExecute(String playAdiId) {
                                        Logcat.e("gpsadid", "id->" + playAdiId);
                                        Utility.setAdvertisingID(SplashSceenActivity.this,playAdiId);
                                        new  CallInstallApi(SplashSceenActivity.this).execute();
                                    }
                                }.execute(SplashSceenActivity.this);
                            }
                        }

                    }catch (Exception e){

                    }
                    try {
                        if (deeplinkObj != null && deeplinkObj.has("loadurl") && !deeplinkObj.isNull("loadurl")) {
                            Utility.setLoadURL(SplashSceenActivity.this, "https://" + deeplinkObj.get("loadurl").toString());
                        }
                    }catch (Exception e){

                    }

                    if (deeplinkObj.has("OpenPage") && deeplinkObj.get("OpenPage").toString().equals("BScreenActivity")) {
                        Utility.setDeeplinkUser(SplashSceenActivity.this, true);
                    }
                }
            }


        } catch (Exception e){
            Logcat.e("json problem",e.toString());
        }

    }

    public static class CallInstallApi extends AsyncTask<String, Integer, JSONObject> {

        Context context;
        CallInstallApi(Context con) {
            context = con;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            JSONObject mainJson = new JSONObject();
            String url = "";
            try {
                url = Utility.getEndp(context) + "/API/InAppWAP/InstallCallBack/Default.aspx?flowName=BScreenActivity";

                mainJson.put("model", Utility.getModel());
                mainJson.put("deviceId", Utility.getDeviceIdFromDevice(context));
                mainJson.put("gpsAdid", Utility.getAdvertisingID(context));
                mainJson.put("platformId", 1);
                mainJson.put("os", "android");
                mainJson.put("osVersion", Utility.getOsVersion());
                mainJson.put("packageName", Utility.getPackageName(context));
                mainJson.put("appVersionCode", Utility.getAppVersionCode());
                mainJson.put("referringLink", URLDecoder.decode(Utility.getReferringLink(context), "UTF-8"));
                mainJson.put("deepLinkObj", Utility.getDeeplinkObject(context));
                mainJson.put("typeOfBuild", Utility.getTypeOfBuild());
                mainJson.put("lang", Locale.getDefault().getLanguage());
                Logcat.e(tag, "url :" + url);
                Logcat.e(tag, "installJson: " + mainJson.toString());


            } catch (Exception e) {
                e.printStackTrace();

            }
            return HttpPostClient.sendHttpPost(url, mainJson);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                Logcat.e(tag, "install result: " + result);

                if (result != null) {
                    readJsonObjectForInstall(result, context);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }
    }
    public static void readJsonObjectForInstall(JSONObject jsonObject, Context context) {
        try {
            Logcat.e(tag, "jsonObject: " + jsonObject.toString());
            if (jsonObject != null) {

                if (jsonObject.has("IDInstall") && !jsonObject.isNull("IDInstall")) {
                    if (jsonObject.getInt("IDInstall") != -1) {
                        Utility.setIdInstall(context, jsonObject.get("IDInstall").toString());
                        Logcat.e(tag, "IdInstall :" + Utility.getIdInstall(context));
                        Utility.setAddToInstall(context, true);
                    }
                }

            }
        } catch (Exception e) {
        }
    }
}