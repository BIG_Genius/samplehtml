
package com.sample.html;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

public class MessageReceiver extends BroadcastReceiver {
    private static final String TAG = "MessageReceiver";

    public MessageReceiver() {
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        try {
            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(action)) {
                Bundle extras = intent.getExtras();
                Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
                Log.e(TAG, "onReceive: " + status);
                switch(status.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        Log.e(TAG, "onReceive: SUCCESS" );
                        String smsMessage = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                        Log.e(TAG, "Retrieved sms code: " + smsMessage);
                        Logcat.e("MessageReceiver", "Retrieved sms code: " + smsMessage);
                        if (smsMessage != null && smsMessage.length() > 0) {
                            ActivityHTML activityHTML = ActivityHTML.getInstance();
                            if (activityHTML != null){
                                VerifyPincode verifyPincode = activityHTML;
                                verifyPincode.fillSMS(smsMessage);
                            }
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        Log.e(TAG, "onReceive: TIMEOUT" );
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception var7) {
            Logcat.e("MessageReceiver", "Exception : " + var7.toString());
        }
    }
}

