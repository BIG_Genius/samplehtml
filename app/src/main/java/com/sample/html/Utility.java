package com.sample.html;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;

import org.json.JSONObject;

import java.net.URLEncoder;

public class Utility {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm == null || cm.getActiveNetworkInfo() == null) ? false : cm
                .getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public static void setDeeplinkUser(Context context, final boolean value){
        PreferenceData.setBooleanPrefs(PreferenceData.KEY_IS_DEEPLINK_USER,context,value);
    }

    public static boolean isDeeplinkUser(Context context){
        return PreferenceData.getBooleanPrefs(PreferenceData.KEY_IS_DEEPLINK_USER,context);
    }

    public static void setLoadURL(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_LOAD_URL, context,value);
    }
    public static String getLoadURL(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_LOAD_URL,context);
    }

    public static void setThankYouURL(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_THANK_YOU_URL, context,value);
    }
    public static String getThankYouURL(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_THANK_YOU_URL,context);
    }

    public static void setReferringLink(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_REFERRING_LINK,context,value);
    }
    public static String getReferringLink(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_REFERRING_LINK,context);
    }

    public static void setPremiumUser(Context context, final boolean value){
        PreferenceData.setBooleanPrefs(PreferenceData.KEY_IS_PREMIUM,context,value);
    }
    public static boolean isPremiumUser(Context context){
        return PreferenceData.getBooleanPrefs(PreferenceData.KEY_IS_PREMIUM,context);
    }

    public static void setDeviceID(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_DEVICE_ID,context,value);
    }
    public static String getDeviceID(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_DEVICE_ID,context);
    }

    public static void setPackageID(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_PACKAGE_ID,context,value);
    }
    public static String getPackageID(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_PACKAGE_ID,context);
    }

    public static void setOpenExt(Context context, final boolean value){
        PreferenceData.setBooleanPrefs(PreferenceData.KEY_OPENEXT,context,value);
    }
    public static boolean isOpenExt(Context context){
        return PreferenceData.getBooleanPrefs(PreferenceData.KEY_OPENEXT,context);
    }

    public static void setAdvertisingID(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_ADVERTISING_ID,context,value);
    }
    public static String getAdvertisingID(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_ADVERTISING_ID,context);
    }

    public static void setDeeplinkObject(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_DEEPLINK_OBJECT,context,value);
    }
    public static String getDeeplinkObject(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_DEEPLINK_OBJECT,context);
    }

    public static void setAddToInstall(Context context, final Boolean value){
        PreferenceData.setBooleanPrefs(PreferenceData.KEY_ADD_TO_INSTALLS,context,value);
    }
    public static boolean getAddToInstall(Context context){
        return PreferenceData.getBooleanPrefs(PreferenceData.KEY_ADD_TO_INSTALLS,context);
    }
    public static void setIdInstall(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_ID_INSTALL,context,value);
    }
    public static String getIdInstall(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_ID_INSTALL,context);
    }
    public static void saveEndp(Context context, final String value){
        PreferenceData.setStringPrefs(PreferenceData.KEY_ENDP,context,value);
    }
    public static String getEndp(Context context){
        return PreferenceData.getStringPrefs(PreferenceData.KEY_ENDP,context);
    }
    public static int getAppVersionCode(){
        return BuildConfig.VERSION_CODE;
    }
    public static int getTypeOfBuild(){
        if (BuildConfig.DEBUG){
            return 0;
        }
        return 1;
    }
    public static void setLogs(final JSONObject deeplinkObj){
        try {

            if (deeplinkObj != null && deeplinkObj.has("logs") && deeplinkObj.get("logs").toString().equalsIgnoreCase("true")) {
                Logcat.ALLOW_LOG = true;
                return;
            }
            Logcat.ALLOW_LOG = false;

        }catch (Exception e){
            Logcat.ALLOW_LOG = false;
        }
    }

    public static String getReferringLinkFromDeeplink(final JSONObject deeplinkObj, final Context context){
        try {
            if (deeplinkObj != null && deeplinkObj.has("~referring_link") ) {
                String referringLink = deeplinkObj.getString("~referring_link").toString();
                if (referringLink != null && !referringLink.equalsIgnoreCase("")){
                    try {
                        setReferringLink(context, URLEncoder.encode(referringLink,"UTF-8"));
                    }catch (Exception e){

                    }
                    return referringLink;
                }
            }
            return "";

        }catch (Exception e){
            Logcat.e("getReferringLinkFromDeeplink", "Exception: "+e.toString());
            return "";
        }
    }

    public static String getPackageName(Context context) {
        String mPackageId = "";
        mPackageId = Utility.getPackageID(context);
        if (mPackageId != null && !mPackageId.isEmpty()){
            return mPackageId;
        } else {
            try {
                if (context != null) {
                    mPackageId = context.getPackageName();
                    Utility.setPackageID(context,mPackageId);
                    return mPackageId;
                } else {
                    return "";
                }
            } catch (Exception e) {
                Logcat.e("getPackageName error", "" + e.toString());
                return "";
            }
        }
    }
    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }
    public static String getModel() {
        return Build.MODEL;
    }
    public static String getDeviceIdFromDevice(Context context) {
        String android_id = "";
        android_id = Utility.getDeviceID(context);
        if (android_id != null && !android_id.isEmpty()){
            return android_id;
        } else {
            android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (android_id != null && !android_id.isEmpty()) {
                Utility.setDeviceID(context,android_id);
                return android_id;
            }
            if (android_id == null) {
                android_id = "";
            }
            return android_id;
        }
    }
    public static String generateAdvertisingId(Context context){
        AdvertisingIdClient.AdInfo advertisingIdClient = null;
        try {
            advertisingIdClient = AdvertisingIdClient.getAdvertisingIdInfo(context);
            return advertisingIdClient.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
